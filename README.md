# R In Epi 2021

Slides and notes from my November 17th, 2021 presentation hosted by RStudio.

- To view the slides, clone the repo and open slides.html.
- To compile the slides, use the Makefile built target `slides.html`.
- Have a nice day.
